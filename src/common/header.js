import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';

import { Link, withRouter } from 'react-router';

import l20n, { Entity } from '@sketchpixy/rubix/lib/L20n';

import {
    Label,
    SidebarBtn,
    Dispatcher,
    NavDropdown,
    NavDropdownHover,
    Navbar,
    Nav,
    NavItem,
    MenuItem,
    Badge,
    Button,
    Icon,
    Grid,
    Row,
    Radio,
    Col } from '@sketchpixy/rubix';

class Brand extends React.Component {
    render() {
        return (
            <Navbar.Header {...this.props}>
                <Navbar.Brand tabIndex='-1'>
                    <a href='#'>
                        <img src='/imgs/common/logo.png' alt='rubix' width='111'/>
                    </a>
                </Navbar.Brand>
            </Navbar.Header>
        );
    }
}

@withRouter class HeaderNavigation extends React.Component {
    render() {
        return (
            <ul className="nav navbar-nav navbar-right pull-right">
                <li className="visible-phone-landscape">
                    <a href="#" id="search-toggle">
                        <i className="fa fa-search"></i>
                    </a>
                </li>
                <li className="dropdown">
                    <a href="#" title="Messages" id="messages" className="dropdown-toggle" data-toggle="dropdown">
                        <i className="glyphicon glyphicon-comment"></i>
                    </a>
                    <ul id="messages-menu" className="dropdown-menu messages" role="menu">
                        <li role="presentation">
                            <a href="#" className="message">
                                <img src="img/1.png" alt=""/>

                                <div className="details">
                                    <div className="sender">Jane Hew</div>
                                    <div className="text">
                                        Hey, John! How is it going? ...
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#" className="message">
                                <img src="img/2.png" alt=""/>

                                <div className="details">
                                    <div className="sender">Alies Rumiancaŭ</div>
                                    <div className="text">
                                        I'll definitely buy this template
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#" className="message">
                                <img src="img/3.png" alt=""/>

                                <div className="details">
                                    <div className="sender">Michał Rumiancaŭ</div>
                                    <div className="text">
                                        Is it really Lore ipsum? Lore ...
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#" className="text-align-center see-all">
                                See all messages <i className="fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
                <li className="dropdown">
                    <a href="#" title="8 support tickets" className="dropdown-toggle" data-toggle="dropdown">
                        <i className="glyphicon glyphicon-globe"></i>
                        <span className="count">8</span>
                    </a>
                    <ul id="support-menu" className="dropdown-menu support" role="menu">
                        <li role="presentation">
                            <a href="#" className="support-ticket">
                                <div className="picture">
                                    <span className="label label-important"><i className="fa fa-bell-o"></i></span>
                                </div>
                                <div className="details">
                                    Check out this awesome ticket
                                </div>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#" className="support-ticket">
                                <div className="picture">
                                    <span className="label label-warning"><i className="fa fa-question-circle"></i></span>
                                </div>
                                <div className="details">
                                    "What is the best way to get ...
                                </div>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#" className="support-ticket">
                                <div className="picture">
                                    <span className="label label-success"><i className="fa fa-tag"></i></span>
                                </div>
                                <div className="details">
                                    This is just a simple notification
                                </div>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#" className="support-ticket">
                                <div className="picture">
                                    <span className="label label-info"><i className="fa fa-info-circle"></i></span>
                                </div>
                                <div className="details">
                                    12 new orders has arrived today
                                </div>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#" className="support-ticket">
                                <div className="picture">
                                    <span className="label label-important"><i className="fa fa-plus"></i></span>
                                </div>
                                <div className="details">
                                    One more thing that just happened
                                </div>
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#" className="text-align-center see-all">
                                See all tickets <i className="fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
                <li className="divider"></li>
                <li className="hidden-xs">
                    <a href="#" id="settings" title="" data-toggle="popover" data-placement="bottom"
                       data-original-title="Settings">
                        <i className="glyphicon glyphicon-cog"></i>
                    </a>
                </li>
                <li className="hidden-xs dropdown">
                    <a href="#" title="Account" id="account" className="dropdown-toggle" data-toggle="dropdown">
                        <i className="glyphicon glyphicon-user"></i>
                    </a>
                    <ul id="account-menu" className="dropdown-menu account" role="menu">
                        <li role="presentation" className="account-picture">
                            <img src="img/2.png" alt=""/>
                            Philip Daineka
                        </li>
                        <li role="presentation">
                            <a href="#" className="link">
                                <i className="fa fa-user"></i>
                                Profile
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="component_calendar.html" className="link">
                                <i className="fa fa-calendar"></i>
                                Calendar
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#" className="link">
                                <i className="fa fa-inbox"></i>
                                Inbox
                            </a>
                        </li>
                    </ul>
                </li>
                <li className="visible-xs">
                    <a href="#" className="btn-navbar" data-toggle="collapse" data-target=".sidebar" title="">
                        <i className="fa fa-bars"></i>
                    </a>
                </li>
                <li className="hidden-xs"><a href="#"><i className="glyphicon glyphicon-off"></i></a></li>
            </ul>
        );
    }
}


export default React.createClass({
    render() {
        return (
            <Grid id='navbar' className="page-header" {...this.props}>
                <Row>
                    <Col xs={12}>
                        <Navbar fluid >
                            <Row>
                                <Col xs={3} visible='xs'>
                                    <SidebarBtn />
                                </Col>
                                <Col xs={6} sm={4}>
                                    <Brand />
                                </Col>
                                <Col xs={3} sm={8} collapseRight className='text-right'>
                                    <HeaderNavigation />
                                </Col>
                            </Row>
                        </Navbar>
                    </Col>
                </Row>
            </Grid>
        );
    }
});
