import React from 'react';

import {
    Sidebar, SidebarNav, SidebarNavItem,
    SidebarControls, SidebarControlBtn,
    LoremIpsum, Grid, Row, Col, FormControl,
    Label, Progress, Icon,
    SidebarDivider
} from '@sketchpixy/rubix';

import { Link, withRouter } from 'react-router';


import ChatComponent from './chat';
import StatisticsComponent from './statistics';
import TimelineComponent from './timeline';
import NotificationsComponent from './notifications';

@withRouter
class ApplicationSidebar extends React.Component {
  handleChange(e) {
    this._nav.search(e.target.value);
  }

  render() {
    return (
      <div>
        <Grid>
          <Row>
            <Col xs={12}>
              <div className='sidebar-nav-container'>
                <SidebarNav style={{marginBottom: 0}} ref={(c) => this._nav = c}>

                  { /** Pages Section */ }
                  <div className='sidebar-header'>PAGES</div>

                  <SidebarNavItem glyph='icon-dripicons-document' name='Menu1'>
                    <SidebarNav>
                      <SidebarNavItem name='SubMenu1' href='/submenu' />
                    </SidebarNav>
                  </SidebarNavItem>

                  { /** Components Section */ }
                  <div className='sidebar-header'>COMPONENTS</div>

                  <SidebarNavItem glyph='icon-simple-line-icons-layers float-right-rtl' name='Panels' href='panels' />
                  <SidebarNavItem glyph='icon-ikons-bar-chart-2 float-right-rtl' name={<span>Charts <Label className='bg-brown50 fg-white'>4</Label></span>}>
                    <SidebarNav>
                      <SidebarNavItem glyph='icon-fontello-chart-area' name='Rubix Charts'>
                        <SidebarNav>
                          <SidebarNavItem name='Line Series' href='/charts/rubix/line' />
                          <SidebarNavItem name='Area Series' href='/charts/rubix/area' />
                          <SidebarNavItem name='Bar + Column Series' href='/charts/rubix/barcol' />
                          <SidebarNavItem name='Mixed Series' href='/charts/rubix/mixed' />
                          <SidebarNavItem name='Pie + Donut Series' href='/charts/rubix/piedonut' />
                        </SidebarNav>
                      </SidebarNavItem>
                      <SidebarNavItem glyph='icon-simple-line-icons-graph' name='Chart.JS' href='/charts/chartjs' />
                      <SidebarNavItem glyph='icon-dripicons-graph-line' name='C3.JS' href='/charts/c3js' />
                      <SidebarNavItem glyph='icon-feather-pie-graph' name='Morris.JS' href='/charts/morrisjs' />
                    </SidebarNav>
                  </SidebarNavItem>

                  <SidebarNavItem href='/timeline' glyph='icon-ikons-time' name='Static Timeline' />
                  <SidebarNavItem href='/interactive-timeline' glyph='icon-fontello-back-in-time' name='Interactive Timeline' />
                  <SidebarNavItem href='/codemirror' glyph='icon-dripicons-code' name='Codemirror' />
                  <SidebarNavItem href='/maps' glyph='icon-ikons-pin-2' name='Maps' />
                  <SidebarNavItem href='/editor' glyph='icon-simple-line-icons-note' name='Editor' />
                  <SidebarNavItem glyph='icon-feather-toggle' name={<span>UI Elements <Label className='bg-deepred fg-white'>7</Label></span>}>
                    <SidebarNav>
                      <SidebarNavItem href='/ui-elements/buttons' glyph='icon-mfizz-oracle' name='Buttons' />
                      <SidebarNavItem href='/ui-elements/dropdowns' glyph='icon-outlined-arrow-down' name='Dropdowns' />
                      <SidebarNavItem href='/ui-elements/tabs-and-navs' glyph='icon-nargela-navigation' name='Tabs &amp; Navs' />
                      <SidebarNavItem href='/ui-elements/sliders' glyph='icon-outlined-three-stripes-horiz' name='Sliders' />
                      <SidebarNavItem href='/ui-elements/knobs' glyph='icon-ikons-chart-3-8' name='Knobs' />
                      <SidebarNavItem href='/ui-elements/modals' glyph='icon-pixelvicon-browser-1' name='Modals' />
                      <SidebarNavItem href='/ui-elements/messenger' glyph='icon-dripicons-message' name='Messenger' />
                    </SidebarNav>
                  </SidebarNavItem>
                  <SidebarNavItem glyph='icon-stroke-gap-icons-Files float-right-rtl' name={<span>Forms <Label className='bg-danger fg-white'>3</Label></span>}>
                    <SidebarNav>
                      <SidebarNavItem glyph='icon-mfizz-fire-alt' href='/forms/controls' name='Controls' />
                      <SidebarNavItem glyph='icon-stroke-gap-icons-Edit' href='/forms/x-editable' name='X-Editable' />
                      <SidebarNavItem glyph='icon-simple-line-icons-magic-wand' href='/forms/wizard' name='Wizard' />
                    </SidebarNav>
                  </SidebarNavItem>
                  <SidebarNavItem glyph='icon-fontello-table' name={<span>Tables <Label className='bg-blue fg-white'>3</Label></span>}>
                    <SidebarNav>
                      <SidebarNavItem href='/tables/bootstrap-tables' glyph='icon-fontello-th-thumb' name='Bootstrap Tables' />
                      <SidebarNavItem href='/tables/datatables' glyph='icon-fontello-th-2' name='Datatables' />
                      <SidebarNavItem href='/tables/tablesaw' glyph='icon-fontello-view-mode' name='Tablesaw' />
                    </SidebarNav>
                  </SidebarNavItem>
                  <SidebarNavItem href='/grid' glyph='icon-ikons-grid-1 float-right-rtl' name='Grid' />
                  <SidebarNavItem href='/calendar' glyph='icon-fontello-calendar-alt' name='Calendar' />
                  <SidebarNavItem glyph='icon-fontello-folder-open-empty' name={<span>File Utilities <Label className='bg-orange fg-darkbrown'>2</Label></span>}>
                    <SidebarNav>
                      <SidebarNavItem href='/file-utilities/dropzone' glyph='icon-stroke-gap-icons-Download' name='Dropzone' />
                      <SidebarNavItem href='/file-utilities/crop' glyph='icon-ikons-crop' name='Image Cropping' />
                    </SidebarNav>
                  </SidebarNavItem>
                  <SidebarNavItem href='/fonts' glyph='icon-fontello-fontsize' name='Fonts' />

                  <SidebarDivider />

                </SidebarNav>
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

class DummySidebar extends React.Component {
  render() {
    return (
      <Grid>
        <Row>
          <Col xs={12}>
            <div className='sidebar-header'>DUMMY SIDEBAR</div>
            <LoremIpsum query='1p' />
          </Col>
        </Row>
      </Grid>
    );
  }
}

@withRouter
export default class SidebarContainer extends React.Component {
  render() {
    return (
      <div id='sidebar' {...this.props}>
        <div id='avatar'>
          <Grid>
            <Row className='fg-white'>
              <Col xs={4} collapseRight>
                <img src='/imgs/app/avatars/avatar0.png' width='40' height='40' />
              </Col>
              <Col xs={8} collapseLeft id='avatar-col'>
                <div style={{top: 23, fontSize: 16, lineHeight: 1, position: 'relative'}}>Anna Sanchez</div>
                <div>
                  <Progress id='demo-progress' value={30} color='#ffffff'/>
                  <a href='#'>
                    <Icon id='demo-icon' bundle='fontello' glyph='lock-5' />
                  </a>
                </div>
              </Col>
            </Row>
          </Grid>
        </div>
        <SidebarControls>
          <SidebarControlBtn bundle='fontello' glyph='docs' sidebar={0} />
          <SidebarControlBtn bundle='fontello' glyph='chat-1' sidebar={1} />
          <SidebarControlBtn bundle='fontello' glyph='chart-pie-2' sidebar={2} />
          <SidebarControlBtn bundle='fontello' glyph='th-list-2' sidebar={3} />
          <SidebarControlBtn bundle='fontello' glyph='bell-5' sidebar={4} />
        </SidebarControls>
        <div id='sidebar-container'>
          <Sidebar sidebar={0}>
            <ApplicationSidebar />
          </Sidebar>
          <Sidebar sidebar={1}>
            <ChatComponent />
          </Sidebar>
          <Sidebar sidebar={2}>
            <StatisticsComponent />
          </Sidebar>
          <Sidebar sidebar={3}>
            <TimelineComponent />
          </Sidebar>
          <Sidebar sidebar={4}>
            <NotificationsComponent />
          </Sidebar>
        </div>
      </div>
    );
  }
}
