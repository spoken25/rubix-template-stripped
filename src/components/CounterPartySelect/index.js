import React from 'react';

export default function (props){
    let counterPartyList = props.counterParties.map((counterParty)=>{
        return(
            <option value={counterParty}  primaryText={counterParty.name} key={`currency-menu-${counterParty.tier}`}/>
        )
    });

    return(
        <select value={props.value} onChange={props.onChange} style={{width: "100%"}}>
            {counterPartyList}
        </select>
    )
}
