/**
 * Created by Max Kudla.
 */

'use strict';

import React from 'react';

export default React.createClass({
    render() {
        return (
            <div className="content container">
                <h2 className="page-title">Default page</h2>
            </div>
        );
    }
});
